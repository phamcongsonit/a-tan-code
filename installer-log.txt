********************************************************************************
* DUPLICATOR-LITE: INSTALL-LOG
* VERSION: 1.2.42
* STEP-1 START @ 03:44:18
* NOTICE: Do NOT post this data to public sites or forums
********************************************************************************
PHP VERSION:	5.6.37 | SAPI: fpm-fcgi
PHP TIME LIMIT:	[0] time limit restriction disabled
PHP MEMORY:	2048M | SUHOSIN: disabled
SERVER:		nginx/1.14.0
DOC ROOT:	/home/news.tintuc60phut.com/public_html
DOC ROOT 755:	true
LOG FILE 644:	true
REQUEST URL:	http://news.tintuc60phut.com/installer.php
SAFE MODE :	0
--------------------------------------
ARCHIVE EXTRACTION
--------------------------------------
NAME:	loaphuong_3eaa1870c76f1f3f3717180829154226_archive.zip
SIZE:	32.53MB
ZIP:	Enabled (ZipArchive Support)

>>> START EXTRACTION:
ZipArchive Object
(
    [status] => 0
    [statusSys] => 0
    [numFiles] => 5359
    [filename] => /home/news.tintuc60phut.com/public_html/loaphuong_3eaa1870c76f1f3f3717180829154226_archive.zip
    [comment] => 
)
File timestamp is 'Current' mode: 2018-09-05 15:44:18
<<< EXTRACTION COMPLETE: true

WEB SERVER CONFIGURATION FILE RESET:
- Backup of .htaccess/web.config made to *.180905154418.orig
- Reset of .htaccess/web.config files

STEP-1 COMPLETE @ 03:44:18 - RUNTIME: 0.6927 sec.


********************************************************************************
* DUPLICATOR-LITE: INSTALL-LOG
* STEP-2 START @ 03:44:35
* NOTICE: Do NOT post to public sites or forums
********************************************************************************
--------------------------------------
DATABASE ENVIRONMENT
--------------------------------------
MYSQL VERSION:	This Server: 10.0.36 -- Build Server: 10.3.8
FILE SIZE:	database.sql (2.71MB) - installer-data.sql (2.71MB)
TIMEOUT:	5000
MAXPACK:	134217728
SQLMODE:	NOT_SET
NEW SQL FILE:	[/home/news.tintuc60phut.com/public_html/installer-data.sql]
COLLATE RESET:	On

--------------------------------------
DATABASE RESULTS
--------------------------------------
ERRORS FOUND:	0
TABLES DROPPED:	0
QUERIES RAN:	17968

bdvn_commentmeta: (0)
bdvn_comments: (0)
bdvn_duplicator_packages: (1)
bdvn_ewwwio_images: (88)
bdvn_links: (0)
bdvn_options: (1788)
bdvn_postmeta: (1844)
bdvn_posts: (165)
bdvn_term_relationships: (25)
bdvn_term_taxonomy: (24)
bdvn_termmeta: (0)
bdvn_terms: (24)
bdvn_usermeta: (50)
bdvn_users: (1)
bdvn_yoast_seo_links: (11865)
bdvn_yoast_seo_meta: (2074)

Removed '26' cache/transient rows

CREATE/INSTALL RUNTIME: 0.7747 sec.
STEP-2 COMPLETE @ 03:44:35 - RUNTIME: 0.7870 sec.


********************************************************************************
* DUPLICATOR-LITE: INSTALL-LOG
* STEP-3 START @ 03:44:38
* NOTICE: Do NOT post to public sites or forums
********************************************************************************
CHARSET SERVER:	utf8
CHARSET CLIENT:	utf8
--------------------------------------
SERIALIZER ENGINE
[*] scan every column
[~] scan only text columns
[^] no searchable columns
--------------------------------------
bdvn_commentmeta^ (0)
bdvn_comments^ (0)
bdvn_duplicator_packages^ (0)
bdvn_ewwwio_images~ (88)
bdvn_links^ (0)
bdvn_options~ (1761)
bdvn_postmeta~ (1844)
bdvn_posts~ (165)
bdvn_term_relationships~ (25)
bdvn_term_taxonomy~ (24)
bdvn_termmeta^ (0)
bdvn_terms~ (24)
bdvn_usermeta~ (50)
bdvn_users~ (1)
bdvn_yoast_seo_links~ (11865)
bdvn_yoast_seo_meta~ (2074)
--------------------------------------
Search1:	'/home/tintuc.loaphuong.info/public_html' 
Change1:	'/home/news.tintuc60phut.com/public_html' 
Search2:	'\/home\/tintuc.loaphuong.info\/public_html' 
Change2:	'\/home\/news.tintuc60phut.com\/public_html' 
Search3:	'%2Fhome%2Ftintuc.loaphuong.info%2Fpublic_html%2F' 
Change3:	'%2Fhome%2Fnews.tintuc60phut.com%2Fpublic_html%2F' 
Search4:	'\home\tintuc.loaphuong.info\public_html' 
Change4:	'/home/news.tintuc60phut.com/public_html' 
Search5:	'//tintuc.loaphuong.info' 
Change5:	'//news.tintuc60phut.com' 
Search6:	'\/\/tintuc.loaphuong.info' 
Change6:	'\/\/news.tintuc60phut.com' 
Search7:	'%2F%2Ftintuc.loaphuong.info' 
Change7:	'%2F%2Fnews.tintuc60phut.com' 
SCANNED:	Tables:16 	|	 Rows:17921 	|	 Cells:85607 
UPDATED:	Tables:3 	|	 Rows:11957 	|	 Cells:11963 
ERRORS:		0 
RUNTIME:	1.089000 sec

====================================
CONFIGURATION FILE UPDATES:
====================================

UPDATED WP-CONFIG: /wp-config.php' (if present)

WEB SERVER CONFIGURATION FILE BASIC SETUP:
- Preparing .htaccess file with basic setup.
Basic .htaccess file edit complete.  If using IIS web.config this process will need to be done manually.

====================================
GENERAL UPDATES & CLEANUP:
====================================

- Created directory wp-snapshots
- Created file wp-snapshots/index.php

====================================
NOTICES
====================================

No Notices Found


STEP 3 COMPLETE @ 03:44:39 - RUNTIME: 1.0965 sec.


