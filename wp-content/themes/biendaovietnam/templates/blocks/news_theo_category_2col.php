<?php
$type_section_news = get_sub_field('type_section_news');
$select_category_small_news = get_sub_field('select_category_small_news');
$select_category_large_news = get_sub_field('select_category_large_news');
?>
<div class="news_category_2col <?php echo ($type_section_news == 'small_right')? 'small_right' :''?>">
    <div class="container">
        <div class="row">
            <div class="has_border">
                <div class="col-md-4 col-sm-5 col-xs-12 <?php echo ($type_section_news == 'small_right')? 'col-md-push-8 col-sm-push-7' :''?>">
                    <?php
                    if($select_category_small_news):
                    $news_small = new WP_Query(array(
                        'post_type' =>  'post',
                        'posts_per_page' =>  3,
                        'cat'   =>  $select_category_small_news
                    ));
                    ?>
                        <div class="box_news_2col box_news_2col_small">
                            <h2 class="title_box_news">
                                <a href="<?php echo get_category_link($select_category_small_news);?>" title="<?php echo get_cat_name($select_category_small_news);?>">
                                    <span><?php echo get_cat_name($select_category_small_news);?></span>
                                </a>
                            </h2>
                            <div class="box_news_2col_list">
                                <?php
                                if($news_small->have_posts()):
                                while($news_small->have_posts()):$news_small->the_post();?>
                                <div class="news_box news_img_title_desc">
                                    <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                                    <?php if(has_post_thumbnail()):?><div class="news_box_img"><div class="img_respon"><?php the_post_thumbnail('medium');?></div></div><?php endif;?>
                                        <div class="news_box_title"><?php the_title();?></div>
                                    </a>
                                    <div class="news_box_desc"><?php the_excerpt();?></div>
                                </div>
                                <?php endwhile;?>
                                <?php else:?>
                                    <p><?php _e('Đang cập nhật...','devvn');?></p>
                                <?php endif; wp_reset_query();?>
                            </div>
                        </div>
                    <?php endif;?>
                </div>
                <div class="col-md-8 col-sm-7 col-xs-12 <?php echo ($type_section_news == 'small_right')? 'col-md-pull-4 col-sm-pull-5' :''?>">
                    <?php
                    if($select_category_large_news):
                        $news_large = new WP_Query(array(
                            'post_type' =>  'post',
                            'posts_per_page' =>  5,
                            'cat'   =>  $select_category_large_news
                        ));
                        ?>
                        <div class="box_news_2col box_news_2col_large">
                            <h2 class="title_box_news">
                                <a href="<?php echo get_category_link($select_category_large_news);?>" title="<?php echo get_cat_name($select_category_large_news);?>">
                                    <span><?php echo get_cat_name($select_category_large_news);?></span>
                                </a>
                            </h2>
                            <div class="box_news_2col_list">
                                <?php
                                if($news_large->have_posts()):
                                    while($news_large->have_posts()):$news_large->the_post();?>
                                        <div class="news_box news_img_title_styl01">
                                            <?php if(has_post_thumbnail()):?><div class="news_box_img"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><div class="img_respon"><?php the_post_thumbnail('medium');?></div></a></div><?php endif;?>
                                            <div class="news_title_desc">
                                                <div class="news_box_title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></div>
                                                <div class="news_box_desc"><?php the_excerpt();?></div>
                                            </div>
                                        </div>
                                    <?php endwhile;?>
                                <?php else:?>
                                    <p><?php _e('Đang cập nhật...','devvn');?></p>
                                <?php endif; wp_reset_query();?>
                            </div>
                        </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
        <div class="border_bottom"></div>
    </div>
</div>
