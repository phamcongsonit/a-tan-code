<?php
$title_videos = get_sub_field('title_videos');
$link_title_videos = get_sub_field('link_title_videos');
$mau_nen_videos = get_sub_field('mau_nen_videos');
$select_category_videos = get_sub_field('select_category_videos');
$number_videos = get_sub_field('number_videos');
if($select_category_videos):
$videos = new WP_Query(array(
    'post_type'         =>  'post',
    'posts_per_page'    =>  $number_videos,
    'category__in'      =>  $select_category_videos
));
if($videos->have_posts()):
?>
<div class="videos_home_section" <?php if($mau_nen_videos):?>style="background-color: <?php echo $mau_nen_videos;?>;" <?php endif;?>>
    <div class="container">
        <div class="row">
            <?php
            $stt = 1;
            while($videos->have_posts()):$videos->the_post();
            if($stt == 1):
            ?>
            <div class="col-sm-12 col-xs-12">
                <div class="news_box videos_big">
                    <div class="videos_big_left">
                        <?php if($title_videos):?><h2>
                            <?php if($link_title_videos):?><a href="<?php echo esc_url($link_title_videos);?>" title="<?php echo $title_videos;?>"><?php endif;?>
                                <?php echo $title_videos;?>
                            <?php if($link_title_videos):?></a><?php endif;?>
                        </h2><?php endif;?>
                        <div class="news_box_title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></div>
                        <div class="news_box_desc"><?php the_excerpt();?></div>
                    </div>
                    <div class="videos_big_right"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><div class="img_respon"><?php the_post_thumbnail('medium');?></div></a></div>
                </div>
            </div>
            <?php else:?>
            <div class="col-md-2 col-sm-4 col-xs-6">
                <div class="news_box news_img_title">
                    <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                        <?php if(has_post_thumbnail()):?><div class="news_box_img"><div class="img_respon"><?php the_post_thumbnail('medium');?></div></div><?php endif;?>
                        <div class="news_box_title"><?php the_title();?></div>
                    </a>
                </div>
            </div>
            <?php endif;?>
            <?php $stt++; endwhile;?>
        </div>
    </div>
</div>
<?php
endif; wp_reset_query();
endif;?>
