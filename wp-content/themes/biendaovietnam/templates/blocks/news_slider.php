<?php
$style_slider = get_sub_field('style_slider');
$select_cat_slider = get_sub_field('select_cat_slider');
$number_slide = get_sub_field('number_slide');
$news_slider = new WP_Query(array(
    'post_type' =>  'post',
    'posts_per_page'    =>  $number_slide,
    'cat'  =>  $select_cat_slider
));
$post_count = $news_slider->post_count;
if($news_slider->have_posts()):
?>
<div class="news_slider <?php echo $style_slider;?>">
    <div class="container">
        <?php if($style_slider == 'style1'):?>
            <div class="news_slider_style2">
                <?php
                $stt = 1;
                while($news_slider->have_posts()):$news_slider->the_post();
                ?>
                <?php if($stt == 1):?>
                    <div class="news_slider_style2_left">
                        <div class="ns_style2_left_infor">
                            <h2 class="h2_style">
                                <a href="<?php echo get_category_link($select_cat_slider);?>" title="<?php echo get_cat_name($select_cat_slider);?>">
                                    <span><?php echo get_cat_name($select_cat_slider);?></span>
                                </a>
                            </h2>
                            <div class="news_box_title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></div>
                            <div class="news_box_desc"><?php the_excerpt();?></div>
                        </div>
                        <div class="ns_style2_left_img">
                            <?php if(has_post_thumbnail()):?><a href="<?php the_permalink();?>" title="<?php the_title();?>"><div class="img_respon"><?php the_post_thumbnail('medium');?></div></a><?php endif;?>
                        </div>
                    </div>
                <?php else:?>
                    <?php if($stt == 2):?><div class="news_slider_style2_right"><div class="ns_style2_slider"><?php endif;?>
                        <div class="ns_style2_box">
                            <div class="news_box news_img_title">
                                <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                                    <?php if(has_post_thumbnail()):?><div class="news_box_img"><div class="img_respon"><?php the_post_thumbnail('medium');?></div></div><?php endif;?>
                                    <div class="news_box_title"><?php the_title();?></div>
                                </a>
                            </div>
                        </div>
                    <?php if($stt == $number_slide || $stt == $post_count):?></div></div><?php endif;?>
                <?php endif;?>
                <?php $stt++; endwhile; wp_reset_query();?>
            </div>
        <?php else:?>
            <?php
            $stt = 1;
            while($news_slider->have_posts()):$news_slider->the_post();
            ?>
                <?php if($stt == 1):?><div class="news_slider_style1_right">
                    <h2 class="h2_style2">
                        <a href="<?php echo get_category_link($select_cat_slider);?>" title="<?php echo get_cat_name($select_cat_slider);?>">
                            <span><?php echo get_cat_name($select_cat_slider);?></span>
                        </a>
                    </h2>
                    <div class="ns_style1_slider firstLoad"><?php endif;?>
                    <div class="ns_style1_box">
                        <div class="news_box news_img_title">
                            <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                                <?php if(has_post_thumbnail()):?><div class="news_box_img"><div class="img_respon"><?php the_post_thumbnail('medium');?></div></div><?php endif;?>
                                <div class="news_box_title"><?php the_title();?></div>
                            </a>
                        </div>
                    </div>
                <?php if($stt == $number_slide || $stt == $post_count):?></div></div><?php endif;?>
            <?php $stt++; endwhile; wp_reset_query();?>
        <?php endif;?>
    </div>
</div>
<?php endif; wp_reset_query();?>